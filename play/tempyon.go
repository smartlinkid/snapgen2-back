package main

import (
	snap2 "bitbucket.org/smartlinkid/snapgen2-back"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats.go"
	"os"
	"strings"
)

const QUEUE_BOOT = "tempyon.boot"

func main() {
	devList := strings.Split(os.Getenv("DEV_LIST"), ",")
	control, e := snap2.New("35.229.151.31", "control", "pz12048kh3")
	if e != nil {
		panic(e)
	}
	e = control.ListenBootClean(func(msg *nats.Msg) {
		fmt.Println("Bootclean", string(msg.Data))
		var dev snap2.DeviceData
		e := json.Unmarshal(msg.Data, &dev)
		if e != nil {
			fmt.Println("parsing failed")
			return
		}

		if !checkExist(devList, dev.Id) {
			fmt.Println("Not exist", dev.Id)
			return
		}
		// turning on
		dev.Timeleft = 0
		dev.Switch = true
		dev.SwitchType = 2
		res, e := control.Switcher(dev)
		if res != nil && !res.Result {
			fmt.Println("Command failed")
			return
		}
		fmt.Println("Command success")
	}, QUEUE_BOOT)
	if e != nil {
		panic(e)
	}
	r := gin.Default()
	r.Run(":8888")
}
func checkExist(devList []string, search string) bool {
	for _, s := range devList {
		if s == search {
			return true
		}
	}
	return false
}
