package main

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/helper"
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"math"
)

type Fix struct {
	Now     int    `json:"now,string"`
	Reality int    `json:"reality,string"`
	Idowner string `json:"idowner"`
}

func (d Fix) getDelta() int {
	return d.Now - d.Reality
}
func main() {
	data, _ := ioutil.ReadFile("fixing.json")
	var x []Fix
	json.Unmarshal(data, &x)
	fmt.Println(x)
	db, e := helper.GetDbManual("sql-proxy.smartlink.id", "pentil", "p3nt1lmuter1029384756", "8806", "citridia_sinadme")
	if e != nil {
		panic(e)
	}
	e = helper.ManualTransaction(db, func(tx *gorm.DB) error {
		for _, fix := range x {
			// update
			e := db.Exec("update data_koin_owner set jumlah = jumlah + ? where idowner = ?", math.Abs(float64(fix.getDelta())), fix.Idowner).Error
			if e != nil {
				fmt.Println("Failed executing update ", fix)
				return e
			}
			var temp model.DataKoinOwner
			db.Where("idowner = ?", fix.Idowner).First(&temp)
			// insert
			e = db.Create(&model.HistoryKoin{
				Idowner:    fix.Idowner,
				Jenis:      1,
				Masuk:      fix.getDelta(),
				Keluar:     0,
				Saldo:      temp.Jumlah,
				RefType:    nil,
				RefId:      nil,
				Keterangan: fmt.Sprintf("Penyesuaian bug koin senilai %d", fix.getDelta()),
			}).Error
			if e != nil {
				fmt.Println("Failed executing insert history", fix)
				return e
			}
			fmt.Println("Fixing ok", fix)
		}

		return nil
	})
	if e != nil {
		fmt.Println(e)
	}
}
