package main

import (
	snap2 "bitbucket.org/smartlinkid/snapgen2-back"
	"fmt"
	"github.com/dgrijalva/jwt-go"
)

type Hook struct {
	snap2.DeviceData
	jwt.Claims
}

func main() {
	claim := snap2.DeviceData{}
	x := jwt.NewWithClaims(jwt.SigningMethodHS512, claim)
	s, e := x.SignedString([]byte("telekbongko"))
	fmt.Println(e)
	fmt.Println(s)

}
