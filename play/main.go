package main

/**
what must be done with nats
1. turning on with timer done
2. turning on without timer done
3. turning off
4. get status
5. listen for status
6. listen status on off via nats streaming
7. getting current work
8. setup phase report

what must be done with endpoint
1. turning on with timer
2. turning on without timer
3. turning off
4. get status
7. getting current work
8. setting webhook

*/

import (
	snap2 "bitbucket.org/smartlinkid/snapgen2-back"
	"fmt"
	"github.com/nats-io/nats.go"
)

var dev *snap2.DeviceControl
var e error
var tl map[string]snap2.DeviceData

func main() {
	nc, e := nats.Connect("nats-iot.smartlink.id", func(options *nats.Options) error {
		options.User = "holopis"
		options.Password = "kontolbaris"
		return nil
	})
	if e != nil {
		panic(e)
	}
	_, _ = nc.Subscribe("maxwell.citridia_sinadme.snap_transaksi_detail", func(msg *nats.Msg) {
		fmt.Println(msg.Subject, string(msg.Data))
	})

	select {}
	//if e != nil {
	//	panic(e)
	//}
	//dev.ListenSerial(func(data map[string]interface{}) {
	//	if data["id"].(string) == "A4CF12B614CB" {
	//		fmt.Println("SERIAL", data["id"], data["text"])
	//	}
	//})
	//dev.ListenBoot(func(msg *nats.Msg) {
	//	var d snap2.DeviceData
	//	json.Unmarshal(msg.Data, &d)
	//	fmt.Println("Device booting", string(msg.Data))
	//}, snap2.Q_LISTENER_DEFAULT)
	//dev.ListenOff(func(msg *nats.Msg) {
	//	fmt.Println("Device turned off", string(msg.Data))
	//}, snap2.Q_LISTENER_DEFAULT)
	//dev.ListenOn(func(msg *nats.Msg) {
	//	fmt.Println("Device turned on", string(msg.Data))
	//}, snap2.Q_LISTENER_DEFAULT)
	//dev.ListenReport(func(msg *nats.Msg) {
	//	//fmt.Println("Device report", string(msg.Data))
	//	//var d snap2.DeviceData
	//	//json.Unmarshal(msg.Data, &d)
	//	//tl[d.Id] = d
	//}, snap2.Q_LISTENER_DEFAULT)
	//dev.ListenDoor(func(msg *nats.Msg) {
	//	fmt.Println("Door report", string(msg.Data))
	//}, snap2.Q_LISTENER_DEFAULT)
	//dev.ListenHeartbeat(func(msg *nats.Msg) {
	//	fmt.Println("heartbeat", string(msg.Data))
	//},"hert")
	//r := gin.New()
	//
	//r.GET("/ont", func(context *gin.Context) {
	//	res, err := dev.Switcher(snap2.DeviceData{
	//		Switch:     true,
	//		SwitchType: 1,
	//		ActiveId:   "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
	//		Timeleft:   30000,
	//		Id:         "CC50E3680D97",
	//	})
	//	if err != nil {
	//		context.String(400, err.Error())
	//	} else {
	//		context.JSON(200, res)
	//	}
	//})
	//r.GET("/ont2", func(context *gin.Context) {
	//	res, err := dev.Switcher(snap2.DeviceData{
	//		Switch:     true,
	//		SwitchType: 1,
	//		ActiveId:   "Sangar",
	//		Timeleft:   30000,
	//		Id:         "CC50E3680D97",
	//	})
	//	if err != nil {
	//		context.String(400, err.Error())
	//	} else {
	//		context.JSON(200, res)
	//	}
	//})
	//r.GET("/ping", func(context *gin.Context) {
	//	res, _ := dev.Ping()
	//	context.JSON(200, res)
	//})
	//r.GET("/pingid", func(context *gin.Context) {
	//	res := dev.GetDeviceDataBatch([]string{"F4CFA2D160CA", "abc", "def"})
	//	context.JSON(200, res)
	//})
	//r.GET("/on", func(context *gin.Context) {
	//	res, err := dev.Switcher(snap2.DeviceData{
	//		Switch:     true,
	//		SwitchType: 2,
	//		ActiveId:   "",
	//		Timeleft:   0,
	//		Id:         "F4CFA2D160CA",
	//	})
	//	if err != nil {
	//		context.String(200, err.Error())
	//	} else {
	//		context.JSON(200, res)
	//	}
	//})
	//r.GET("/on/:id", func(context *gin.Context) {
	//	id := context.Param("id")
	//	res, err := dev.Switcher(snap2.DeviceData{
	//		Switch:     true,
	//		SwitchType: 2,
	//		ActiveId:   "",
	//		Timeleft:   0,
	//		Id:         id,
	//	})
	//	if err != nil {
	//		context.String(200, err.Error())
	//	} else {
	//		context.JSON(200, res)
	//	}
	//})
	//r.POST("/flash/:id", func(context *gin.Context) {
	//	url := context.PostForm("url")
	//	dev.Flash(context.Param("id"), url)
	//})
	//r.GET("/off/:id", func(context *gin.Context) {
	//	id := context.Param("id")
	//	res, err := dev.Switcher(snap2.DeviceData{
	//		Id:       id,
	//		ActiveId: "XXX",
	//		Switch:   false,
	//	})
	//	if err != nil {
	//		context.String(200, err.Error())
	//	} else {
	//		context.JSON(200, res)
	//	}
	//})
	//r.GET("/info", func(context *gin.Context) {
	//	result, err := dev.GetDeviceData("F4CFA2D160CA")
	//	if err != nil {
	//		context.String(200, err.Error())
	//	} else {
	//		context.JSON(200, result)
	//	}
	//})
	//r.GET("/wifi", func(context *gin.Context) {
	//	result, err := dev.GetDeviceWifi("F4CFA2D160CA")
	//	if err != nil {
	//		context.String(200, err.Error())
	//	} else {
	//		context.JSON(200, result)
	//	}
	//})
	//r.GET("/wifiid", func(context *gin.Context) {
	//	res := dev.GetDeviceWifiBatch([]string{"F4CFA2D160CA", "abc", "def"})
	//	context.JSON(200, res)
	//})
	//r.Run(":8081")
}
