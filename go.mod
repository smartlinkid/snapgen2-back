module bitbucket.org/smartlinkid/snapgen2-back

go 1.14

require (
	bitbucket.org/smartlinkid/smartlink-go-base v1.0.146
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.2
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.11
	github.com/kr/pretty v0.2.1 // indirect
	github.com/nats-io/nats-streaming-server v0.18.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/nats-io/stan.go v0.7.0
)
