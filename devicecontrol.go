package snap2

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"time"
)

const (
	S_REPORT_SWITCH     = "report.switch"
	S_REPORT_SWITCH_OFF = "report.switch.off"
	S_REPORT_DOOR       = "report.door"
	S_REPORT_STAT       = "report.stat"
	S_REPORT_BOOT       = "report.boot"
	S_REPORT_BOOT_CLEAN = "report.bootclean"
	S_REPORT_SERIAL     = "report.serial"
	S_REPORT_HEARTBEAT  = "report.heartbeat"
	CMD_PING            = "cmd.ping"

	REPLY_WAIT         = 15
	Q_MAIN             = "q.main"
	Q_LISTENER_DEFAULT = "q.listener.default"
)

type DevProcedure struct {
	Idbrand string `json:"idbrand"`
	Seq     int    `json:"seq"`
	Pin     int    `json:"pin"`
	Value   int    `json:"value"`
	Board   string `json:"board"`
}

type DevBrand struct {
	Id        string `json:"id"`
	BrandMerk string `json:"brand_merk"`
	BrandType string `json:"brand_type"`
	Jenis     int    `json:"jenis"`
	Hapus     int    `json:"hapus"`
}

type DevBrandList struct {
	Idbrand  string `json:"idbrand"`
	Iddevice string `json:"iddevice"`
}

type DevList struct {
	Id    string `json:"id"`
	Board string `json:"board"`
}

type DeviceControl struct {
	Nc             *nats.Conn
	Enc            *nats.EncodedConn
	Sc             stan.Conn
	DefaultListen  func(d *DeviceData, action int)
	DebugDeviceIds []string
}

type DeviceData struct {
	Idhistory    uint64         `json:"idhistory" gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	Switch       bool           `json:"sw"`               // true on false off
	SwitchType   int            `json:"st"`               // 1 on with timer, 2 on without timer
	ActiveId     string         `json:"aid" gorm:"INDEX"` // insert active id. will be sent when reporting timeleft, on, and off
	Timeleft     int64          `json:"tl"`               // millisec, set this when turning on will be the tier if switch type is 1.
	Id           string         `json:"id" gorm:"INDEX"`  // device id
	Pow          float64        `json:"pow"`              // power
	Cur          float64        `json:"cur"`              // current
	Vol          float64        `json:"vol"`              // vol
	Duration     int64          `json:"dur"`              // initial duration
	Lat          int64          `json:"lat"`
	Door         bool           `json:"door"` // true: open, false: closed
	Ver          int            `json:"ver"`  // firmware version
	Stamp        time.Time      `json:"stamp"`
	FwVer        string         `json:"fw_ver"`
	DevProcedure []DevProcedure `json:"proc"`
	PwrB         bool           `json:"pwr_b"`
	EnabDoor     bool           `json:"enab_door"`
	HwVer        string         `json:"hw_ver"`
	At           int64          `json:"at"`
}
type DeviceWifi struct {
	Id   string  `json:"id"`
	Ssid string  `json:"ssid"`
	Rssi float64 `json:"rssi"`
	Ip   string  `json:"ip"`
	Up   int64   `json:"up"`
	Lat  int64   `json:"lat"`
	Ver  int     `json:"ver"`
}

type CommandResult struct {
	Result  bool `json:"res"`
	Message int  `json:"msg"`
}

func (t *CommandResult) getStringMessage() string {
	var x = map[int]string{
		0:   "Success",
		101: "Device is used",
	}
	return x[t.Message]
}

func New(url string, user string, pass string) (*DeviceControl, error) {
	nc, e := nats.Connect(url, func(options *nats.Options) error {
		options.User = user
		options.Password = pass
		return nil
	})
	if e != nil {
		return nil, e
	}
	enc, e := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if e != nil {
		return nil, e
	}
	d := &DeviceControl{
		Nc:  nc,
		Enc: enc,
	}
	e = d.setupReplier()
	if e != nil {
		return nil, e
	}
	return d, e
}

func (d *DeviceControl) setupReplier() error {
	h := func(msg *nats.Msg) {
		var clear byte
		switch msg.Subject {
		case S_REPORT_BOOT:
			clear = 1
		case S_REPORT_SWITCH:
			clear = 2
		case S_REPORT_SWITCH_OFF:
			clear = 3
		case S_REPORT_DOOR:
			clear = 4
		case S_REPORT_BOOT_CLEAN:
			clear = 5
		}
		send := []byte{clear}
		var device DeviceData
		e := json.Unmarshal(msg.Data, &device)
		if e != nil {
			fmt.Println(e)
		}
		subj := fmt.Sprintf("cmd.clear.%s", device.Id)
		_ = d.Nc.Publish(subj, send)
		if d.DefaultListen != nil {
			d.DefaultListen(&device, int(clear))
		}
	}
	var e error
	_, e = d.Nc.QueueSubscribe(S_REPORT_DOOR, Q_MAIN, h)
	if e != nil {
		return e
	}
	_, e = d.Nc.QueueSubscribe(S_REPORT_SWITCH_OFF, Q_MAIN, h)
	if e != nil {
		return e
	}
	_, e = d.Nc.QueueSubscribe(S_REPORT_SWITCH, Q_MAIN, h)
	if e != nil {
		return e
	}
	_, e = d.Nc.QueueSubscribe(S_REPORT_BOOT, Q_MAIN, h)
	if e != nil {
		return e
	}
	_, e = d.Nc.QueueSubscribe(S_REPORT_BOOT_CLEAN, Q_MAIN, h)
	if e != nil {
		return e
	}
	_, e = d.Nc.QueueSubscribe(S_REPORT_HEARTBEAT, Q_MAIN, func(msg *nats.Msg) {
		// got heartbeat
		var data string
		data = string(msg.Data)
		if len(data) > 12 {
			var deviceData DeviceData
			ee := json.Unmarshal(msg.Data, &deviceData)
			if ee != nil {
				fmt.Println(ee)
				return
			}
			ee = d.Nc.Publish(fmt.Sprintf("cmd.heartbeat.%s", deviceData.Id), []byte(data))
			if ee != nil {
				fmt.Println(ee)
				return
			}
		} else {
			ee := d.Nc.Publish(fmt.Sprintf("cmd.heartbeat.%s", data), []byte(data))
			if ee != nil {
				fmt.Println(ee)
				return
			}
		}
	})
	return nil
}

func (d *DeviceControl) GetStan(clusterId string, clientId string) (*stan.Conn, error) {
	sc, e := stan.Connect(clusterId, clientId, stan.NatsConn(d.Nc))
	if e != nil {
		return nil, e
	}
	return &sc, nil
}

func (d *DeviceControl) Flash(id string, link string) error {
	random, _ := uuid.NewRandom()
	reply := fmt.Sprintf("reply.%s", random)
	e := d.Nc.PublishRequest(fmt.Sprintf("cmd.flash.%s", id), reply, []byte(link))
	if e != nil {
		return e
	}
	s, e := d.Nc.SubscribeSync(reply)
	if e != nil {
		return e
	}
	_, e = s.NextMsg(REPLY_WAIT * time.Second)
	if e != nil {
		return e
	}
	return nil
}
func (d *DeviceControl) Switcher(data DeviceData) (*CommandResult, error) {
	data.At = time.Now().UnixMilli()
	random, _ := uuid.NewRandom()
	reply := fmt.Sprintf("reply.%s", random)
	e := d.Enc.PublishRequest(fmt.Sprintf("cmd.switch.%s", data.Id), reply, data)
	if e != nil {
		return nil, e
	}
	s, e := d.Nc.SubscribeSync(reply)
	if e != nil {
		return nil, e
	}
	msg, e := s.NextMsg(REPLY_WAIT * time.Second)
	if e != nil {
		return nil, e
	}
	var res CommandResult
	e = json.Unmarshal(msg.Data, &res)
	if e != nil {
		return nil, e
	}
	if !res.Result {
		return &res, errors.New(res.getStringMessage())
	}
	return &res, nil
}

func (d *DeviceControl) GetDeviceData(id string) (*DeviceData, error) {
	random, _ := uuid.NewRandom()
	reply := fmt.Sprintf("reply.%s", random)
	start := time.Now()
	e := d.Nc.PublishRequest(fmt.Sprintf("cmd.data.%s", id), reply, []byte("qwe"))
	if e != nil {
		return nil, e
	}
	s, e := d.Nc.SubscribeSync(reply)
	if e != nil {
		return nil, e
	}
	msg, e := s.NextMsg(REPLY_WAIT * time.Second)
	if e != nil {
		return nil, e
	}
	var result DeviceData
	e = json.Unmarshal(msg.Data, &result)
	if e != nil {
		return nil, e
	}
	end := time.Now()
	lat := end.Sub(start)
	result.Lat = lat.Milliseconds()
	return &result, nil
}

func (d *DeviceControl) GetDeviceDataBatch(ids []string) map[string]*DeviceData {
	list := make(map[string]*DeviceData)
	// listen
	random, _ := uuid.NewRandom()
	reply := fmt.Sprintf("reply.%s", random)
	for _, id := range ids {
		list[id] = nil
	}
	start := time.Now()
	sub, e := d.Nc.Subscribe(reply, func(msg *nats.Msg) {
		var result DeviceData
		e := json.Unmarshal(msg.Data, &result)
		if e != nil {
			return
		}
		end := time.Now()
		result.Lat = end.Sub(start).Milliseconds()
		list[result.Id] = &result
	})

	if e != nil {
		fmt.Errorf(e.Error())
		return nil
	}
	// broadcast
	for _, id := range ids {
		d.Nc.PublishRequest(fmt.Sprintf("cmd.data.%s", id), reply, []byte("qwe"))
	}
	// sleep
	time.Sleep(REPLY_WAIT * time.Second)
	// unsubscribe
	_ = sub.Unsubscribe()

	//wg := sync.WaitGroup{}
	//wg.Add(len(ids))
	//for _, id := range ids {
	//	go func(i string, d *DeviceControl) {
	//		res, e := d.GetDeviceData(i)
	//		if e != nil {
	//			list[i] = nil
	//		} else {
	//			list[i] = res
	//		}
	//		wg.Done()
	//	}(id, d)
	//}
	//wg.Wait()
	return list
}

func (d *DeviceControl) GetDeviceWifi(id string) (*DeviceWifi, error) {
	random, _ := uuid.NewRandom()
	reply := fmt.Sprintf("reply.%s", random)
	start := time.Now()
	e := d.Nc.PublishRequest(fmt.Sprintf("cmd.wifi.%s", id), reply, []byte("qwe"))
	if e != nil {
		return nil, e
	}
	s, e := d.Nc.SubscribeSync(reply)
	if e != nil {
		return nil, e
	}
	msg, e := s.NextMsg(REPLY_WAIT * time.Second)
	if e != nil {
		return nil, e
	}
	end := time.Now()
	var result DeviceWifi
	e = json.Unmarshal(msg.Data, &result)
	result.Lat = end.Sub(start).Milliseconds()
	fmt.Println(string(msg.Data))
	if e != nil {
		return nil, e
	}
	return &result, nil
}

func (d *DeviceControl) GetDeviceWifiBatch(ids []string) map[string]*DeviceWifi {
	list := make(map[string]*DeviceWifi)
	// listen
	random, _ := uuid.NewRandom()
	reply := fmt.Sprintf("reply.%s", random)
	for _, id := range ids {
		list[id] = nil
	}
	start := time.Now()
	sub, e := d.Nc.Subscribe(reply, func(msg *nats.Msg) {
		var result DeviceWifi
		e := json.Unmarshal(msg.Data, &result)
		if e != nil {
			return
		}
		end := time.Now()
		result.Lat = end.Sub(start).Milliseconds()
		list[result.Id] = &result
	})

	if e != nil {
		fmt.Errorf(e.Error())
		return nil
	}
	// broadcast
	for _, id := range ids {
		d.Nc.PublishRequest(fmt.Sprintf("cmd.wifi.%s", id), reply, []byte("qwe"))
	}
	// sleep
	time.Sleep(10 * time.Second)
	// unsubscribe
	_ = sub.Unsubscribe()
	return list
}

func (d *DeviceControl) Ping() (map[string]DeviceData, error) {
	list := make(map[string]DeviceData)
	random, _ := uuid.NewRandom()
	reply := fmt.Sprintf("reply.%s", random)
	e := d.Nc.PublishRequest(CMD_PING, reply, []byte{1})
	if e != nil {
		return nil, e
	}
	s, e := d.Nc.SubscribeSync(reply)
	if e != nil {
		return nil, e
	}
	for {
		res, e := s.NextMsg(REPLY_WAIT * time.Second)
		if e != nil {
			break
		}
		var x DeviceData
		e = json.Unmarshal(res.Data, &x)
		if e == nil {
			list[x.Id] = x
		}
	}
	return list, nil
}
func (d *DeviceControl) ListenHeartbeat(h nats.MsgHandler, queue string) error {
	_, e := d.Nc.QueueSubscribe(S_REPORT_HEARTBEAT, queue, h)
	return e
}
func (d *DeviceControl) ListenBoot(h nats.MsgHandler, queue string) error {
	_, e := d.Nc.QueueSubscribe(S_REPORT_BOOT, queue, h)
	return e
}
func (d *DeviceControl) ListenBootClean(h nats.MsgHandler, queue string) error {
	_, e := d.Nc.QueueSubscribe(S_REPORT_BOOT_CLEAN, queue, h)
	return e
}
func (d *DeviceControl) ListenOn(h nats.MsgHandler, queue string) error {
	_, e := d.Nc.QueueSubscribe(S_REPORT_SWITCH, queue, h)
	return e
}
func (d *DeviceControl) ListenOff(h nats.MsgHandler, queue string) error {
	_, e := d.Nc.QueueSubscribe(S_REPORT_SWITCH_OFF, queue, h)
	return e
}

func (d *DeviceControl) ListenDoor(h nats.MsgHandler, queue string) error {
	_, e := d.Nc.QueueSubscribe(S_REPORT_DOOR, queue, h)
	return e
}

func (d *DeviceControl) ListenReport(h nats.MsgHandler, queue string) error {
	_, e := d.Nc.QueueSubscribe(S_REPORT_STAT, queue, h)
	return e
}
func (d *DeviceControl) ListenSerial(p func(data map[string]interface{})) error {
	_, e := d.Nc.Subscribe(S_REPORT_SERIAL, func(msg *nats.Msg) {
		mapie := make(map[string]interface{})
		mapie["timestamp"] = time.Now()
		mapie["id"] = msg.Reply
		mapie["subject"] = msg.Subject
		mapie["text"] = string(msg.Data)
		p(mapie)
	})
	if e != nil {
		return e
	}
	return nil
}
